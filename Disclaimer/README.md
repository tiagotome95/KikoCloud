# Haftungsausschluss


## Inhalt des Onlineangebotes

Der Autor übernimmt keinerlei Gewähr für die Aktualität, Richtigkeit und Vollständigkeit der bereitgestellten Informationen auf unserer Website. Haftungsansprüche gegen den Autor, welche sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden, sind grundsätzlich ausgeschlossen, sofern seitens des Autors kein nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt.
Alle Angebote sind freibleibend und unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.


## Verweise und Links

Bei direkten oder indirekten Verweisen auf fremde Webseiten (“Hyperlinks”), die außerhalb des Verantwortungsbereiches des Autors liegen, würde eine Haftungsverpflichtung ausschließlich in dem Fall in Kraft treten, in dem der Autor von den Inhalten Kenntnis hat und es ihm technisch möglich und zumutbar wäre, die Nutzung im Falle rechtswidriger Inhalte zu verhindern.
Der Autor erklärt hiermit ausdrücklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zukünftige Gestaltung, die Inhalte oder die Urheberschaft der verlinkten/verknüpften Seiten hat der Autor keinerlei Einfluss. Deshalb distanziert er sich hiermit ausdrücklich von allen Inhalten aller verlinkten /verknüpften Seiten, die nach der Linksetzung verändert wurden. Diese Feststellung gilt für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie für Fremdeinträge in vom Autor eingerichteten Gästebüchern, Diskussionsforen, Linkverzeichnissen, Mailinglisten und in allen anderen Formen von Datenbanken, auf deren Inhalt externe Schreibzugriffe möglich sind. Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige Veröffentlichung lediglich verweist.


## Urheber- und Kennzeichenrecht

Der Autor ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, von ihm selbst erstellte Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zurückzugreifen.
Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind!
Das Copyright für veröffentlichte, vom Autor selbst erstellte Objekte bleibt allein beim Autor der Seiten. Eine Vervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung des Autors nicht gestattet.


## Datenschutz

Sofern innerhalb des Internetangebotes die Möglichkeit zur Eingabe persönlicher oder geschäftlicher Daten (Emailadressen, Namen, Anschriften) besteht, so erfolgt die Preisgabe dieser Daten seitens des Nutzers auf ausdrücklich freiwilliger Basis. Die Inanspruchnahme und Bezahlung aller angebotenen Dienste ist – soweit technisch möglich und zumutbar – auch ohne Angabe solcher Daten bzw. unter Angabe anonymisierter Daten oder eines Pseudonyms gestattet. Die Nutzung der im Rahmen des Impressums oder vergleichbarer Angaben veröffentlichten Kontaktdaten wie Postanschriften, Telefon- und Faxnummern sowie Emailadressen durch Dritte zur Übersendung von nicht ausdrücklich angeforderten Informationen ist nicht gestattet. Rechtliche Schritte gegen die Versender von sogenannten Spam-Mails bei Verstössen gegen dieses Verbot sind ausdrücklich vorbehalten.


## Hinweise zur Registrierung

Die Registrierung auf den Dienst [KikoCloud](https://tst95.de) erfordert die Eingabe von ein benutzerdefinierten `Benutzername` und ein sicheres `Kennwort` (deine Daten sind so sicher wie dein Kennwort). Die Eingabe von E-Mail-Adresse ist aus Datenschutzgründen _nicht pflichtig_, aber für das zurücksetzen des Kennwortes, z.B.: bei Vergessenheit, erforderlich. Sollten Sie auf den Gedanken kommen, für die Registrierung eine Pseudo-Email-Adresse zu verwenden, löschen Sie diese (Pseudo-Email-Adresse) nach abgeschlossene Registrierung bzw. bei Ihre ErstAnmeldung aus Ihrem Profil, so verhindern Sie dass der potentieller Besitzer ihrer Pseudo-Email-Adresse die Kontrolle über Ihren KikoCloud-Konto (in dem er auf `Kennwort zurücksetzen` drückt) übernimmt.
Von der Registrierung auf [KikoCloud](https://tst95.de) bis zur Aktivierung (Freischaltung) des Kontos durch den Administrator, kann einige Zeit vergehen (von wenige Sekunden bis zu maximal einen Tag), Sie erhalten eine E-Mail sobald Ihr Konto aktiviert (freigeschalten) wurde.


## Pflichten der Nutzerinnen und Nutzer des Dienstes

Sollten Sie jemals Performance- oder StabilitätsProbleme des Dienstes [KikoCloud](https://tst95.de) bemerken, oder bisher unentdeckte Sicherheitslücken in der Infrastruktur entdecken, so sind Sie als Nutzerin oder Nutzer des Dienstes KikoCloud dazu verpflichtet, diese umgehend, bei den [Administrator](mailto:tiagotome95@sapo.pt) zu melden. Dies gilt auch für das Erfahren von nicht WünschensWerte  Inhalte, Verhalten/Vergehen (auch bei Mobbing), kriminelle Machenschaften (z.B. Betrug) oder sonstiges unmoralisches Verhalten innerhalb von KikoCloud, egal ob diese von **dich** Selbst (als Täter) oder von Jemand anderes aus stammen. **"MitWissenschaft ohne handeln wird Bestraft!"**

Alle Nutzerinnen und Nutzer von [KikoCloud](https://tst95.de) sind verpflichtet den Dienst frei von Kriegsverherrlichende, Kinderpornografische, Rassistische, Diskriminierende oder auf jegliche Art Umwelt-, Tier-, oder Menschenverachtende Inhalte, zu halten.

Schlechtes Verhalten führt zur Verbannung und Löschung des BenutzerKontos, sowie alle darin enthaltene Daten. Unmoralisches Verhalten führt zu eine sofortige Sperrung des BenuterKontos und kann (je nach SchwereGrad des Verhaltens/Vergehens) zusätzlich zu eine Anzeige bei den zuständigen Behörden führen, dies kann strafrechtliche Maßnahmen zur Folge haben.

Bei Probleme oder Fragen rund um den Dienst [KikoCloud](https://tst95.de) erreichen Sie uns unter der E-Mail-Adresse [tiagotome95@sapo.pt](mailto:tiagotome95@sapo.pt) bei Eingabe von ein passenden Betreff, wie z.B.: **KikoCloud - Problem bei der Konfiguration von WebDAV** oder **KikoCloud - Anfrage für die Aktivierung der Kochbuch-App**. Außerdem erreichen Sie den Administrator in der Talk-App von [KikoCloud](https://tst95.de) unter den Namen `Tiago`.

## Kontrolle über den Zugriff auf Benutzerdaten

Daten, die explizit und bereitwillig von einem Benutzer hochgeladen werden, sollten unter der endgültigen Kontrolle des Benutzers stehen. Die Benutzer sollten entscheiden können, wer direkten Zugriff auf ihre Daten gewährt und mit welchen Berechtigungen und Lizenzen dieser Zugriff gewährt werden soll.

[KikoCloud](https://tst95.de) ist ein freier Cloud-Dienst, der auf die aktuellste Stabile Version von [NextCloud](https://nextcloud.com/) basiert und auf ein VPS-Server mit dem freien Betriebssystem Debian GNU/Linux betrieben wird. Die Konfiguration des Servers, sowie die zum Warten des Servers verwendete Software, die von **KikoCloud** entwickelt wurde, gepflegt und verwendet wird um den Cloud-Dienst zu Betreiben, ist natürlich genauso  **Freie Software** wie "die Basis" [NextCloud](https://nextcloud.com/). Das bedeutet, dass Sie jederzeit die **Quellen** von KikoCloud überprüfen können, um sicherzustellen dass der Server ihre persönliche DatenschutzAnforderungen entspricht, sollte es nicht der Fall sein, lassen Sie es uns Wissen, nur so können wir unsere KikoCloud (sowie unser Basis-System [NextCloud](https://nextcloud.com/) selbst) verbessern und dazu beitragen unsere Daten besser zu schützen, aber auch die Stabilität und Performance des Servers zu erhöhen. Sie sind gerne dazu Willkommen unser 1-Mann-AdministrationsTeam dabei zu helfen [KikoCloud](https://tst95.de) zu verbessern und dazu beitragen bessere Übersetzungen der Inhalte des Dienstes, aber auch der Dokumentation und Quellen von [NextcloudServerConfig](https://gitlab.com/tiagotome95/NextcloudServerConfig), das Projekt hinter KikoCloud, anzubieten.

Dieser Server ist mittels Datenverschlüsselung geschützt. Das wird mithilfe von `ecryptfs` erreicht. Da VPS-server (genauso wie andere Arten von mietbare, entfernte Server, ohne physicalischen Zugriff) im Normal-Fall keine Festplattenverschlüsselung unterstützen, arbeitet der Administrator im Moment daran die Festplatte des Servers aufzuteilen, um das Betriebssystem und alle  bisher datenverschlüsselte Daten nachträglich auf eine sicherere, mithilfe von `LUKS` verschlüsselte Partition der Festplatte umzuziehen. Für die Eingabe des Kennworts, dass für das entschlüsseln der Festplattenverschlüsselung beim Boot-Vorgang notwendig ist, soll der mini SSH-Server Dropbear verwendet werden. Diese Änderungen bringen KikoCloud auf dem höchsten Sicherheits-Level.

Dieser Server wird in Zukunft mittels Festplattenverschlüsselung geschützt.

[Deine Daten, sicher mit NextCloud.](https://nextcloud.com/yourdata)

Danke für euer Verständnis :)

Der Administrator,

[Tiago Simões Tomé](mailto:tiagotome95@sapo.pt)
